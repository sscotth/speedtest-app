# Speedtest

[![Screenshot](http://i.imgur.com/U4ZGk11.png)](https://github.com/adolfintel/speedtest)

No Flash, No Java, No Websocket, No Bullshit.

This is a very lightweight Speedtest implemented in Javascript, using XMLHttpRequest and Web Workers.

## Try it

[Take a Speedtest](http://speedtest.fdossena.com)

## Compatibility

Only modern browsers are supported (IE11, latest Edge, latest Chrome, latest Firefox, latest Safari).

## Installation

Ready for testing via https://yourcloudronserver/#/appstore/io.sscotth.speedtestcloudron?version=0.0.5 or `cloudron install --appstore-id io.sscotth.speedtestcloudron@0.0.5`.
