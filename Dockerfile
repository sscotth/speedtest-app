FROM cloudron/base:0.10.0
MAINTAINER Scott Humphries <docker@sscotth.io>

# define source directory
RUN mkdir -p /app/code
WORKDIR /app/code

# get the source
ENV VERSION 4.2.9
RUN wget "https://github.com/adolfintel/speedtest/archive/${VERSION}.tar.gz" -O - \
    | tar -xz -C /app/code --strip-components=1

# configure Apache
RUN rm /etc/apache2/sites-enabled/* \
    && sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf \
    && sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf \
    && a2disconf other-vhosts-access-log \
    && echo "Listen 8000" > /etc/apache2/ports.
ADD apache2.conf /etc/apache2/sites-available/speedtest.conf
RUN a2ensite speedtest

# Setting up index.html
RUN cp "example${SPEEDTEST_EXAMPLE:-4}.html" index.html

# startup
ADD start.sh /app/code/start.sh
CMD [ "/app/code/start.sh" ]
